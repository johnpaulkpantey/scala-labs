// Recursion
/*
def factorial(n: BigInt): BigInt = {
  def factorialAcc(n: BigInt, acc: BigInt): BigInt={
    if(n<=1) acc
    else factorialAcc(n-1, n* acc)
  }
  factorialAcc(n,1)
}
factorial(200)

// code
def fibonnaci(n: BigInt): BigInt={
  if (n<=1)1
  else fibonnaci(n-1) + fibonnaci(n-1)
}
//fibonnaci(2)
def fabonnaciTailRec(n: BigInt): BigInt={
  def fibonacciAcc(n: BigInt, previous: BigInt, current: BigInt): BigInt = {
    if (n <= 1) current
    else fibonacciAcc(n - 1, current, current + previous) + fibonnaci(n - 1)
  }
  fibonacciAcc(n, 1, 1)
}


def nTimes(n: Int, x: Int, f: Int => Int): Int ={
  if(n<=0) x
  else nTimes(n-1, f(x),f)
}
//nTimes(3,2, _ +1)
*/
val list: List[Int] = List(1,2,34,56,7,8,9)
list.reverse


//maps
val l = List(1,2,3,4,5,6,7,8,9,10)
// The two functions below are the same
def multiplyByThree: Int => Int = _* 3
def multiplyByTwo: Int => Int = (x: Int) => x * 2

l  map multiplyByThree
l.map(multiplyByTwo)
l.map(x => multiplyByTwo(x))
l.filterNot(_ <7)
l.filterNot((x: Int) => x <7)
l.map(_ * 2).filter(_ < 6)

val l2 = List("Calvin","is", "Terrible")
l2.map(_.toLowerCase)
l2.map(_.toUpperCase)
l2.map(x => "Hi" + 2)
l2.flatMap(_.toUpperCase)

val l3 = List(List(1,2), List(3,4), List(5,6))
//l3.flatMap(_ * 2).map(_*2)

for (x <- l) yield x * 2 //same as below
l.map(_ * 2)

for (x <- l if x < 6) yield  x *2 //same as below
l.filter(_ <6).map(_ * 2)

for (
  l <- l3;
  x <- l if x > 2
) yield x * 2

l.foldLeft(0)(_ + _)
l.foldRight(0)(_ - _)
l.fold(0)(_ + _ )
l.reduce(_ + _)
