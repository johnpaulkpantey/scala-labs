package thursday.lab1

/*
class Baz[A]  // An invariant class : Cannot assign subtype to supertype and vice versa
class Foo[+A] // A covariant class: Assign subtype to supertype only
class Bar[-A] // A contravariant class: Assign supertype to subtype only
*/
class ListOfInt[Int] {

    def count(items: ListOfInt) : Int= count(items)
//    def contains()


}
