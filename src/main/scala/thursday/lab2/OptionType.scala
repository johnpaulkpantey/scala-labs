package thursday.lab2

import scala.annotation.targetName
import scala.util.Try

object OptionType {
  def main(args: Array[String]): Unit = {
    println(/(3, 0))
    println(sqrt(0))
    print(getInputs)
  }
  @targetName("division")
//  def /(a: Int, b: Int): Option[Int] = Try(a/b).toOption
  def /(a: Int, b: Int): Option[Any] = if (b !=0) then Some(a/b) else None
  def sqrt(a: Int): Option[Double] = if (a < 0) then Some(math.sqrt(a)) else None

  def getInputs = {
    val list : List[Int] = List(1,2,4,6)
    list.map(x => /(x,3)).filter(_ != None)
  }
}
