package monday.lab1

import java.time.{LocalDate, LocalDateTime, ZonedDateTime}
import java.time.format.DateTimeFormatter
import java.util.Date

object LabWork {
  def main(args: Array[String]): Unit = {
    println(s"Temp in Fahrenheit is ${celsiusToFah(100)}")
    println(convertDates("05/05/96"))
    regexExpres("bin:x:1:1:bin:/bin:/sbin/nologin")
  }
  def celsiusToFah( c: Double): Double =  (c * 9/5) + 32

  def convertDates(dateStr: String): Unit = {
//  Write a function that converts a String containing a date in the form dd / mm / yy
//  into a full representation of the date.
//    For example  01 / 02 / 15 will be 1st February 2015
//      Assume dates will not go back further than needed to convert a 2 digit year to a 4 digit year.
  val splitDateStr = dateStr.split("/")
  val (d, m, y) = (splitDateStr(0), splitDateStr(1), splitDateStr(2))

  println(s"${d.toInt}${getDaySub(d.toInt)} ${getMonth(m.toInt)} 20$y")
//  splitDateStr.foreach(println)
}
  def getMonth(monthInt : Int): String = {
    monthInt match {
      case 1 => "January"
      case 2 => "February"
      case 3 => "March"
      case 4 => "April"
      case 5 => "May"
      case 6 => "June"
      case 7 => "July"
      case 8 => "August"
      case 9 => "September"
      case 10 => "October"
      case 11 => "November"
      case 12 => "December"
    }
  }
    def getDaySub(dayInt: Int): String ={
      dayInt match {
        case 1 => "st"
        case 2 => "nd"
        case 3 => "rd"
        case _ => "th"
      }
  }
    def regexExpres(str: String): Unit ={
      /* Test cases
      root:x:0:0:root:/root:/bin/bash
      bin:x:1:1:bin:/bin:/sbin/nologin
      daemon:x:2:2:daemon:/sbin:/sbin/nologin
      adm:x:3:4:adm:/var/adm:/sbin/nologin
      lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
      sync:x:5:0:sync:/sbin:/bin/sync
      */
      val reg = """(\w{2,}):(\w):(\d):(\d):(\w{2,}):(/\w{2,}){1,}:(/\w{2,}/\w{2,})""".r
      val reg(username,password,userid,groupId,description,home_directory,shell)= str
      print(username,password,userid,groupId,description,home_directory,shell)
    }
}
