import java.io.File
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashMap

object FunctionalCollections extends App {
  val (files, dirs) = new File(".")
    .listFiles
    .toList
    .filter(!_.isHidden)
    .map(_.toString.drop(2))
    .partition("(\\w{2,})\\.(\\w{2,})".r.matches(_))

  println(dirs)
  println(generateFileDetails(files))
  println(generateListOfFileByOrder(files, "asc"))
  println(generateFilenameAndSizeInMap(files))
  println(rearrangeFilesBasedOnFirstLetter(dirs))


  def generateFileDetails(files: List[String]): List[(String, Long)] =
    files.map(f => (f, new File(s"./src/main/scala/dayThree/$f").length()))

  def generateListOfFileByOrder(files: List[String], order: "asc" | "desc") = {
    val sortFunc = order match
      case "asc" => (a: (String, Long), b: (String, Long)) => a._2 < b._2
      case "desc" => (a: (String, Long), b: (String, Long)) => a._2 > b._2

    files.map(f => (f, new File(s"./$f").length()))
      .sortWith(sortFunc).map(_._1)
  }

  def generateFilenameAndSizeInMap(files: List[String]) =
    files.map(f => (f, new File(s"./$f").length())).toMap

  def rearrangeFilesBasedOnFirstLetter(dirs: List[String]) = {
    val maps = new mutable.HashMap[Char, ListBuffer[String]]()
    dirs.foreach(s => {
        if (!maps.contains(s.head)) maps.addOne(s.head, ListBuffer(s))
        else maps.update(s.head, maps(s.head) += s)
    })
    maps
  }
}
  
