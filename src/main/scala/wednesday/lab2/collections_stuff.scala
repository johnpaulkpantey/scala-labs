package wednesday.lab2

import scala.annotation.tailrec

object collections_stuff extends App{

  /*
  *  @Question 1 - > Removing an element from a collection is an operation that is not directly supported by the main Sequence based collection type.
  *                  Investigate the methods provided by Sequence collections, and use them to write a function remove(coll: Seq, idx: Int)
  *                  that removes the element at index position idx from the collection coll.
  *                  Have your collection operate with both mutable or immutable collections, by returning a new collection with the element removed.
  */
  def remove(coll:Seq[Any], idx:Int): Seq[Any] = {
    coll.slice(0,idx) ++ coll.slice(idx+1,coll.length)
  }
  val list1 = List(1,2,3,4,5,6,7,8)
  val listRem = remove(list1,9)
  println(listRem)

  /*
  * @Question2 - > Write a program that plays the UK National Lottery. This involves selecting 6 random integer numbers between 1 and 49.
  *                Clearly the numbers selected must be unique. Investigate the Random type for selecting numbers,
  *                and choose an appropriate collection type to store the numbers. Display the numbers in ascending order.
  *                If you are feeling brave, you could try out the numbers that your program selects... Good luck!
   */
  import scala.util.Random
  
  def lottoNums:Set[Int] = {
    @tailrec
    def lottoNums_2(rand: Set[Int]): Set[Int] ={
      if(rand.size==6) rand
      else lottoNums_2(rand + Random.between(1, 50))
    }
    lottoNums_2(Set[Int]())
  }
  
//  val lottoNums = for (a <- 1 to 6) yield {Random.between(1, 50)}
//  
  println(lottoNums.toList.sorted)
 


}
