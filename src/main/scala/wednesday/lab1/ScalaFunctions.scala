package wednesday.lab1

import scala.annotation.tailrec

object ScalaFunctions {

  def main(args: Array[String]): Unit = {
//    println(Calculations("add" )) // add
//    sumOfNumbersBetweenTwoNumbersAnB(10,)
//    println(concatenateAStringTwice(4))
  }

  def Calculations(operation: String) = {
    def calculateNow(x: Int, y: Int) = {
      if (operation == "add") (x: Int, y: Int) => x + y
      else if (operation == "subtract") (x: Int, y: Int) => x - y
      else (x: Int, y: Int) => math.pow(x, y).toInt
    }

    calculateNow(3, 4)
  }

  def isPrimeNumber(x: Int): Unit = {
      @tailrec
      def checkPrime(x: Int, max: Int): Unit ={
        if (x > 1) {
          if (x/2 == 0) checkPrime(x + 1, max)

        }
      }
      checkPrime(10, 2)


//    if (x > 1) {
//      var s = 0
//      for (s <- 2 to (x / 2) + 1) {
//        if (x % s != 0) {
//          println(s"$s is Prime")
//        }
//
//      }
//
//    }
  }
  //Returns the sum of the numbers between two numbers a and b
  def sumOfNumbersBetweenTwoNumbersAnB(a: Int, b: Int): Int = a+ b

  //Concatenate a given string n times
//  def concatenateAStringTwice(n: String): String = {
//    def concatenateAStringNTwice(str: String): String ={
//      if (str =="") str
//      else concatenateAStringNTwice("Hello")
////        str.concat(str)
//    }
//  }

  def findLenghtOfString(str: String) ={

  }
}
