package tuesday.lab1


object patternMatching {
  def main(args: Array[String]): Unit = {
//    println(getUsers("senior citizen"))
    println(matchNumbers(10))
  }

  def getUsers(userType: String): String =
    userType match {
      case "junior" => "Only 2 books allowed to be borrowed"
      case "regular" => "Only 5 books allowed to be borrowed"
      case "senior citizen" => "Only 7 books allowed to be borrowed"
    }


  def matchNumbers(number: Int): String = {
    number match {
      case x if x == 0 => "< 0"
      case x if 0 until 18 contains x => "0<=number<=18"
      case x if 19 until 35 contains x => "19<=number<=35"
      case x if 36 until 65 contains x => "36<=number<=54"
      case x if x > 65 => ">65"
    }
  }

}
