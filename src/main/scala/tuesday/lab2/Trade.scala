package tuesday.lab2

abstract class Trade (val ID: String,  var _price: Double ){
//Write a Trade class. A Trade has an ID, a Symbol (such as "AAPL" or "IBM"), a quantity and a price, for example, it might be created as follows
//  require(quantity > 0)
  def price = _price // Getter
  def price_= (value: Double) = if (price >= 0) _price = value //Setter

//  override def toString() = s"ID: ${ID}, Symbol: ${Symbol}, Price: ${price}"
}
//object Trade{
//  //Using companion to instantiate the class
//  def apply(id: String, sym: String,qty: Int,prc: Double) = new Trade(id,prc)
//}
class EquityTrade( id: String, val Symbol: String, val quantity: Int, price: Double) extends Trade( id, price){
  def isExecutable = true
  def value() = quantity * price
  override def toString() = s"ID: ${ID}, Symbol: ${Symbol}, Quantity: ${quantity}, Price: ${price}"
}
class FXTrade(id: String, price: Double) extends Trade( id, price){
  def isExecutable = false
  override def toString() = s"ID: ${ID} Price: ${price}"
}

class Transaction( id: String, Symbol: String, quantity: Int, price: Double) extends  EquityTrade( id, Symbol, quantity, price) with FeePayable with Taxable {
def amount() = super.value() + flateFee + tax
}
trait FeePayable{
  val flateFee = 10
}
trait Taxable{
  val tax= 12.5/100
}
